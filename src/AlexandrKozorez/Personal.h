#pragma once

#include "Employee.h"

class Personal : public Employee, public WorkTime
{
	protected:
		int base;
	public:
		Personal() {}
		~Personal() {}
		void setBase(int base) { this->base = base; }
		int getBase() const { return base; }
		double salary_type_1(int worktime, int base) override
		{
			return worktime*base*ratio3;
		}
		void calculate()
		{
			this->payment = salary_type_1(worktime,base);
		}
};

class Cleaner : public Personal
{
	public:
		Cleaner(int id = 0, string name = "", int base = 0, int worktime = 0, int payment = 0)
		{
			this->id = id; 
			this->name = name; 
			this->base = base;
			this->worktime = worktime;
			this->payment = payment;
		}
		~Cleaner() {}
};

class Driver : public Personal
{
	public:
		Driver(int id = 0, string name = "", int base = 0, int worktime = 0, int payment = 0)
		{
			this->id = id;
			this->name = name;
			this->base = base;
			this->worktime = worktime;
			this->payment = payment;
		}
		~Driver() {}
};