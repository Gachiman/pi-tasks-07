#pragma once
#include "Employee_Interface.h"
using namespace std;

class Manager : public Employee, public Project
{
protected:
	string projectTitle;
	double participation;
	int projBudget;
public:
	Manager() : Employee(), projectTitle(""), participation(.0), projBudget(0) {};
	Manager(int id, string name, int worktime, double payment, string projectTitle, double participation, int projBudget) :
		Employee(id, name, worktime, payment)
	{
		this->projectTitle = projectTitle;
		this->participation = participation;
		this->projBudget = projBudget;
	}
	Manager(const Manager& mngr) : Employee(mngr)
	{
		projectTitle = mngr.projectTitle;
		participation = mngr.participation;
		projBudget = mngr.projBudget;
	}

	double payForProject(int projBudget, double participation) override
	{
		return projBudget * participation;
	}

	void calc() override
	{
		setPayment(payForProject(projBudget, participation));
	}

	void showInfo(ostream& stream) override
	{
		stream << "\n--------------------\nID: " << getID() << "\nName: " << getName() << "\nPosition: Manager" << endl;
		stream << "Project Title: " << projectTitle << "\nProject Budget: " << projBudget << " rub\nParticipation: " << participation << endl;
		stream << "Work Time: " << getWorkTime() << " hours" << endl;
		stream << "Salary: " << getPayment() << " rub\n--------------------" << endl;
	}
};

class ProjectManager : public Manager, public Heading
{
protected:
	int subNumber;
	int feeForOne;
public:
	ProjectManager() : Manager(), subNumber(0), feeForOne(0) {};
	ProjectManager(int id, string name, int worktime, double payment, string projectTitle, double participation, int projBudget,
		int subNumber, int feeForOne) : Manager(id, name, worktime, payment, projectTitle, participation, projBudget)
	{
		this->subNumber = subNumber;
		this->feeForOne = feeForOne;
	}
	ProjectManager(const ProjectManager& pm) : Manager(pm)
	{
		subNumber = pm.subNumber;
		feeForOne = pm.feeForOne;
	}

	void showInfo(ostream& stream) override
	{
		stream << "\n--------------------\nID: " << getID() << "\nName: " << getName() << "\nPosition: Project Manager" << endl;
		stream << "Project Title: " << projectTitle << "\nProject Budget: " << projBudget << " rub\nParticipation: " << participation << endl;
		stream << "Subordinates Number: " << subNumber << "\nFee for one: " << feeForOne << " rub\nWork Time: " << getWorkTime() << " hours" << endl;
		stream << "Salary: " << getPayment() << " rub\n--------------------" << endl;
	}

	int payForSubordinates(int feeForOne, int number) override
	{
		return feeForOne * number;
	}

	void calc() override
	{
		setPayment(payForSubordinates(feeForOne, subNumber) + payForProject(projBudget, participation));
	}
};

class SeniorManager : public ProjectManager
{
private:
	int projNum;
public:
	SeniorManager() : ProjectManager(), projNum(0) {};
	SeniorManager(int id, string name, int worktime, double payment, string projectTitle, double participation, int projBudget,
		int subNumber, int feeForOne, int projNum) :
		ProjectManager(id, name, worktime, payment, projectTitle, participation, projBudget, subNumber, feeForOne)
	{
		this->projNum = projNum;
	}
	SeniorManager(const SeniorManager& sm) : ProjectManager(sm)
	{
		projNum = sm.projNum;
	}

	double payForProject(int projBudget, double participation) override
	{
		return projBudget * participation * projNum;
	}

	void showInfo(ostream& stream) override
	{
		stream << "\n--------------------\nID: " << getID() << "\nName: " << getName() << "\nPosition: Senior Manager" << endl;
		stream << "Projects Titles: " << projectTitle << "\nProjects Budget: " << projBudget << " rub\nParticipation in projects: " << participation << endl;
		stream << "Projects Number: " << projNum << endl;
		stream << "Subordinates Number: " << subNumber << "\nFee for one: " << feeForOne << " rub\nWork Time: " << getWorkTime() << " hours" << endl;
		stream << "Salary: " << getPayment() << " rub\n--------------------" << endl;
	}
};