#pragma once
#include "Employee_Interface.h"
using namespace std;

class Personal : public Employee, public WorkTime
{
protected:
	int base;
public:
	Personal() : Employee(), base(0) {};
	Personal(int id, string name, int worktime, double payment, int base) : Employee(id, name, worktime, payment), base(base) {};
	Personal(const Personal& prsnl) : Employee(prsnl), base(prsnl.base) {};

	int payForHours(int worktime, int base) override
	{
		return worktime * base;
	}
	void calc() override
	{
		setPayment(payForHours(getWorkTime(), base));
	}
};

class Cleaner : public Personal
{
public:
	Cleaner() : Personal() {};
	Cleaner(int id, string name, int worktime, double payment, int base) : Personal(id, name, worktime, payment, base) {};
	Cleaner(const Cleaner& clnr) : Personal(clnr) {};
	void showInfo(ostream& stream) override
	{
		stream << "\n--------------------\nID: " << getID() << "\nName: " << getName() << "\nPosition: Cleaner" << endl;
		stream << "Work Time: " << getWorkTime() << " hours\nBase: " << base << " rub/hour\nSalary: " << getPayment() << " rub\n--------------------" << endl;
	}
};

class Driver : public Personal
{
public:
	Driver() : Personal() {};
	Driver(int id, string name, int worktime, double payment, int base) : Personal(id, name, worktime, payment, base) {};
	Driver(const Driver& dr) : Personal(dr) {};
	void showInfo(ostream& stream) override
	{
		stream << "\n--------------------\nID: " << getID() << "\nName: " << getName() << "\nPosition: Driver" << endl;
		stream << "Work Time: " << getWorkTime() << " hours\nBase: " << base << " rub/hour\nSalary: " << getPayment() << "\n--------------------" << endl;
	}
};