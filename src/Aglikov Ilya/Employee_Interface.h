#pragma once
#include <string>
#include <iostream>
using namespace std;

class Employee
{
private:
	int id, worktime;
	double payment;
	string name;
public:
	Employee() : id(0), worktime(0), payment(0.0), name("") {};
	Employee(int id, string name, int worktime, double payment)
	{
		this->id = id;
		this->name = name;
		this->worktime = worktime;
		this->payment = payment;
	}
	Employee(const Employee& emp)
	{
		id = emp.id;
		name = emp.name;
		worktime = emp.worktime;
		payment = emp.payment;
	}
	void setWorkTime(int hours) { worktime = hours; }
	void setPayment(double payment) { this->payment = payment; }
	int getID() const { return id; }
	string getName() const { return name; }
	int getWorkTime() const { return worktime; }
	double getPayment() const { return payment; }
	virtual void showInfo(ostream& stream) = 0;
	virtual void calc() = 0;
};

class WorkTime
{
public:
	virtual int payForHours(int worktime, int base) = 0;
};

class Project
{
public:
	virtual double payForProject(int projBudget, double participation) = 0;
};

class Heading
{
public:
	virtual int payForSubordinates(int feeForOne, int number) = 0;
};