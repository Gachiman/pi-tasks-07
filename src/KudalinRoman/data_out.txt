
--------------------
ID: 1
Name: Ivanov I. I.
Position: Driver
Work Time: 50 hours
Base: 200 rub/hour
Salary: 10000
--------------------

--------------------
ID: 2
Name: Petrova G. I.
Position: Cleaner
Work Time: 50 hours
Base: 170 rub/hour
Salary: 8500 rub
--------------------

--------------------
ID: 3
Name: Sergeev S. V.
Position: Programmer
Project Title: App-078-x-02
Project Budget: 500000 rub
Participation: 0.1
Work Time: 50 hours
Base: 300 rub/hour
Salary: 65000 rub
--------------------

--------------------
ID: 4
Name: Botkin D. S.
Position: Programmer
Project Title: App-078-x-02
Project Budget: 500000 rub
Participation: 0.1
Work Time: 50 hours
Base: 310 rub/hour
Salary: 65500 rub
--------------------

--------------------
ID: 5
Name: Demidov A. D.
Position: Tester
Project Title: App-078-x-02
Project Budget: 500000 rub
Participation: 0.1
Work Time: 50 hours
Base: 310 rub/hour
Salary: 65500 rub
--------------------

--------------------
ID: 6
Name: Artemov N. V.
Position: Team Leader
Project Title: App-078-x-02
Project Budget: 500000 rub
Participation: 0.15
Subordinates Number: 3
Fee For One: 200 rub
Work Time: 50 hours
Base: 330 rub
Salary: 92100 rub
--------------------

--------------------
ID: 7
Name: Losev D. T.
Position: Manager
Project Title: App-078-x-02
Project Budget: 500000 rub
Participation: 0.2
Work Time: 50 hours
Salary: 100000 rub
--------------------

--------------------
ID: 8
Name: Nikitin A. A.
Position: Project Manager
Project Title: App-078-x-02
Project Budget: 500000 rub
Participation: 0.25
Subordinates Number: 5
Fee for one: 300 rub
Work Time: 50 hours
Salary: 126500 rub
--------------------

--------------------
ID: 9
Name: Gorelov I. L.
Position: Programmer
Project Title: Sftwr-1pq	
Project Budget: 500000 rub
Participation: 0.1
Work Time: 50 hours
Base: 315 rub/hour
Salary: 65750 rub
--------------------

--------------------
ID: 10
Name: Novikov Y. Y.
Position: Programmer
Project Title: Sftwr-1pq
Project Budget: 500000 rub
Participation: 0.1
Work Time: 50 hours
Base: 320 rub/hour
Salary: 66000 rub
--------------------

--------------------
ID: 11
Name: Morozov V. I.
Position: Tester
Project Title: Sftwr-1pq
Project Budget: 500000 rub
Participation: 0.1
Work Time: 50 hours
Base: 315 rub/hour
Salary: 65750 rub
--------------------

--------------------
ID: 12
Name: Kirillov A. V.
Position: Team Leader
Project Title: Sftwr-1pq
Project Budget: 500000 rub
Participation: 0.15
Subordinates Number: 3
Fee For One: 220 rub
Work Time: 50 hours
Base: 330 rub
Salary: 92160 rub
--------------------

--------------------
ID: 13
Name: Mokeev S. P.
Position: Manager
Project Title: Sftwr-01pq
Project Budget: 500000 rub
Participation: 0.2
Work Time: 50 hours
Salary: 100000 rub
--------------------

--------------------
ID: 14
Name: Pavlov A. D.
Position: Project Manager
Project Title: Sftwr-01pq
Project Budget: 500000 rub
Participation: 0.25
Subordinates Number: 5
Fee for one: 330 rub
Work Time: 50 hours
Salary: 126650 rub
--------------------

--------------------
ID: 15
Name: Nosov V. I.
Position: Senior Manager
Projects Titles: App-078-x-02, Sftwr-01pq
Projects Budget: 500000 rub
Participation in projects: 0.1
Projects Number: 2
Subordinates Number: 12
Fee for one: 300 rub
Work Time: 50 hours
Salary: 103600 rub
--------------------

--------------------
ID: 16
Name: Tolmachev I. P.
Position: Programmer
Project Title: VrtRlt-25zero1
Project Budget: 700000 rub
Participation: 0.1
Work Time: 50 hours
Base: 320 rub/hour
Salary: 86000 rub
--------------------

--------------------
ID: 17
Name: Zhegalkin V. Y.
Position: Programmer
Project Title: VrtRlt-25zero1
Project Budget: 700000 rub
Participation: 0.1
Work Time: 50 hours
Base: 315 rub/hour
Salary: 85750 rub
--------------------

--------------------
ID: 18
Name: Nikolaev E. N.
Position: Tester
Project Title: VrtRlt-25zero1
Project Budget: 700000 rub
Participation: 0.1
Work Time: 50 hours
Base: 325 rub/hour
Salary: 86250 rub
--------------------

--------------------
ID: 19
Name: Domrachev S. V.
Position: Team Leader
Project Title: VrtRlt-25zero1
Project Budget: 700000 rub
Participation: 0.15
Subordinates Number: 3
Fee For One: 280 rub
Work Time: 50 hours
Base: 330 rub
Salary: 122340 rub
--------------------

--------------------
ID: 20
Name: Pontryagin K. D.
Position: Manager
Project Title: VrtRlt-25zero1
Project Budget: 700000 rub
Participation: 0.2
Work Time: 50 hours
Salary: 140000 rub
--------------------

--------------------
ID: 21
Name: Semenov V. D.
Position: Project Manager
Project Title: VrtRlt-25zero1
Project Budget: 700000 rub
Participation: 0.25
Subordinates Number: 5
Fee for one: 350 rub
Work Time: 50 hours
Salary: 176750 rub
--------------------

--------------------
ID: 22
Name: Chapaev V. A.
Position: Programmer
Project Title: ?dx_1
Project Budget: 700000 rub
Participation: 0.1
Work Time: 50 hours
Base: 340 rub/hour
Salary: 87000 rub
--------------------

--------------------
ID: 23
Name: Rylkov R. R.
Position: Programmer
Project Title: ?dx_1
Project Budget: 700000 rub
Participation: 0.1
Work Time: 50 hours
Base: 325 rub/hour
Salary: 86250 rub
--------------------

--------------------
ID: 24
Name: Stepanov S. R.
Position: Tester
Project Title: ?dx_1
Project Budget: 700000 rub
Participation: 0.1
Work Time: 50 hours
Base: 330 rub/hour
Salary: 86500 rub
--------------------

--------------------
ID: 25
Name: Vdovin A. G.
Position: Team Leader
Project Title: ?dx_1
Project Budget: 700000 rub
Participation: 0.15
Subordinates Number: 3
Fee For One: 280 rub
Work Time: 50 hours
Base: 345 rub
Salary: 123090 rub
--------------------

--------------------
ID: 26
Name: Bolshakov D. D.
Position: Manager
Project Title: ?dx_1
Project Budget: 700000 rub
Participation: 0.2
Work Time: 50 hours
Salary: 140000 rub
--------------------

--------------------
ID: 27
Name: Studin V. D.
Position: Project Manager
Project Title: ?dx_1
Project Budget: 700000 rub
Participation: 0.25
Subordinates Number: 5
Fee for one: 400 rub
Work Time: 50 hours
Salary: 177000 rub
--------------------

--------------------
ID: 28
Name: Korchagin R. I.
Position: Senior Manager
Projects Titles: VrtRlt-25zero1, ?dx_1
Projects Budget: 700000 rub
Participation in projects: 0.1
Projects Number: 2
Subordinates Number: 12
Fee for one: 350 rub
Work Time: 50 hours
Salary: 144200 rub
--------------------

--------------------
ID: 29
Name: Fedorov D. F.
Position: Driver
Work Time: 50 hours
Base: 250 rub/hour
Salary: 12500
--------------------

--------------------
ID: 30
Name: Volontarev I. I.
Position: Driver
Work Time: 50 hours
Base: 265 rub/hour
Salary: 13250
--------------------

--------------------
ID: 31
Name: Vadimov S. R.
Position: Programmer
Project Title: qq_02
Project Budget: 750000 rub
Participation: 0.2
Work Time: 50 hours
Base: 330 rub/hour
Salary: 166500 rub
--------------------

--------------------
ID: 32
Name: Golikov D. T.
Position: Tester
Project Title: qq_02
Project Budget: 750000 rub
Participation: 0.2
Work Time: 50 hours
Base: 330 rub/hour
Salary: 166500 rub
--------------------

--------------------
ID: 33
Name: Astorbaev T. T.
Position: Team Leader
Project Title: qq_02
Project Budget: 750000 rub
Participation: 0.2
Subordinates Number: 2
Fee For One: 290 rub
Work Time: 50 hours
Base: 350 rub
Salary: 168080 rub
--------------------

--------------------
ID: 34
Name: Valerianov G. R.
Position: Manager
Project Title: qq_02
Project Budget: 750000 rub
Participation: 0.1
Work Time: 50 hours
Salary: 75000 rub
--------------------

--------------------
ID: 35
Name: Stasov N. M.
Position: Project Manager
Project Title: qq_02
Project Budget: 750000 rub
Participation: 0.2
Subordinates Number: 4
Fee for one: 400 rub
Work Time: 50 hours
Salary: 151600 rub
--------------------

--------------------
ID: 36
Name: Molostov T. R.
Position: Programmer
Project Title: -xxp-
Project Budget: 750000 rub
Participation: 0.11
Work Time: 50 hours
Base: 700 rub/hour
Salary: 117500 rub
--------------------

--------------------
ID: 37
Name: Romanov D. T.
Position: Tester
Project Title: -xxp-
Project Budget: 750000 rub
Participation: 0.09
Work Time: 50 hours
Base: 650 rub/hour
Salary: 100000 rub
--------------------

--------------------
ID: 38
Name: Antohin L. G.
Position: Team Leader
Project Title: -xxp-
Project Budget: 750000 rub
Participation: 0.2
Subordinates Number: 2
Fee For One: 250 rub
Work Time: 50 hours
Base: 750 rub
Salary: 188000 rub
--------------------

--------------------
ID: 39
Name: Gorshkov V. P.
Position: Manager
Project Title: -xxp-
Project Budget: 750000 rub
Participation: 0.2
Work Time: 50 hours
Salary: 150000 rub
--------------------

--------------------
ID: 40
Name: Loshkarev N. M.
Position: Project Manager
Project Title: -xxp-
Project Budget: 750000 rub
Participation: 0.3
Subordinates Number: 4
Fee for one: 400 rub
Work Time: 50 hours
Salary: 226600 rub
--------------------

--------------------
ID: 41
Name: Likholat M. I.
Position: Senior Manager
Projects Titles: qq_02, -xxp-
Projects Budget: 750000 rub
Participation in projects: 0.1
Projects Number: 2
Subordinates Number: 5
Fee for one: 700 rub
Work Time: 50 hours
Salary: 153500 rub
--------------------
