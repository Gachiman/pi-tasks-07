#pragma once
#include "Interface_Employee.h"
using namespace std;

class Engineer: public Employee, public WorkTime, public Project
{
protected:
	string projectTitle;
	double participation;
	int base;
	int projBudget;
public:
	Engineer() : Employee(), projectTitle(""), participation(.0), base(0), projBudget(0) {};
	Engineer(int id, string name, int worktime, double payment, string projectTitle, double participation, int base, int projBudget) :
		Employee(id, name, worktime, payment)
	{
		this->projectTitle = projectTitle;
		this->participation = participation;
		this->base = base;
		this->projBudget = projBudget;
	}
	Engineer(const Engineer& eng) : Employee(eng)
	{
		projectTitle = eng.projectTitle;
		participation = eng.participation;
		base = eng.base;
		projBudget = eng.projBudget;
	}

	int payForHours(int worktime, int base) override
	{
		return worktime * base;
	}
	double payForProject(int projBudget, double participation) override
	{
		return projBudget * participation;
	}

	void calc() override
	{
		setPayment(payForHours(getWorkTime(), base) + payForProject(projBudget, participation));
	}

};

class Programmer : public Engineer
{
public:
	Programmer(): Engineer() {};
	Programmer(int id, string name, int worktime, double payment, string projectTitle, double participation, int base, int projBudget): 
	Engineer(id, name, worktime, payment, projectTitle, participation, base, projBudget) {};

	Programmer(const Programmer& prog): Engineer(prog) {};

	void showInfo(ostream& stream) override
	{
		stream << "\n--------------------\nID: " << getID() << "\nName: " << getName() << "\nPosition: Programmer" << endl;
		stream << "Project Title: " << projectTitle << "\nProject Budget: " << projBudget << " rub\nParticipation: " << participation << endl;
		stream << "Work Time: " << getWorkTime() << " hours" << endl;
		stream << "Base: " << base << " rub/hour\nSalary: " << getPayment() << " rub\n--------------------" << endl;
	}
};

class Tester : public Engineer
{
public:
	Tester() : Engineer() {};
	Tester(int id, string name, int worktime, double payment, string projectTitle, double participation, int base, int projBudget) :
	Engineer(id, name, worktime, payment, projectTitle, participation, base, projBudget) {};

	Tester(const Tester& test) : Engineer(test) {};

	void showInfo(ostream& stream) override
	{
		stream << "\n--------------------\nID: " << getID() << "\nName: " << getName() << "\nPosition: Tester" << endl;
		stream << "Project Title: " << projectTitle << "\nProject Budget: " << projBudget << " rub\nParticipation: " << participation << endl;
		stream << "Work Time: " << getWorkTime()<< " hours" << endl;
		stream << "Base: " << base << " rub/hour\nSalary: " << getPayment() << " rub\n--------------------" << endl;
	}
};

class TeamLeader : public Programmer, public Heading
{
private:
	int subNumber;
	int feeForOne;
public:
	TeamLeader() : Programmer(), subNumber(0), feeForOne(0) {};
	TeamLeader(int id, string name, int worktime, double payment, string projectTitle, double participation, int base, int projBudget,
			   int subNumber, int feeForOne) :
		Programmer(id, name, worktime, payment, projectTitle, participation, base, projBudget)
	{
		this->subNumber = subNumber;
		this->feeForOne = feeForOne;
	}
	TeamLeader(const TeamLeader& tl) : Programmer(tl)
	{
		subNumber = tl.subNumber;
		feeForOne = tl.feeForOne;
	}

	void showInfo(ostream& stream) override
	{
		stream << "\n--------------------\nID: " << getID() << "\nName: " << getName() << "\nPosition: Team Leader" << endl;
		stream << "Project Title: " << projectTitle << "\nProject Budget: " << projBudget << " rub\nParticipation: " << participation << endl;
		stream << "Subordinates Number: " << subNumber << "\nFee For One: " << feeForOne << " rub\nWork Time: " << getWorkTime()<< " hours" << endl;
		stream << "Base: " << base << " rub\nSalary: " << getPayment()<< " rub\n--------------------" << endl;
	}

	int payForSubordinates(int feeForOne, int number) override
	{
		return feeForOne * number;
	}

	void calc() override
	{
		setPayment(payForHours(getWorkTime(), base) + payForProject(projBudget, participation) 
		          + payForSubordinates(subNumber, feeForOne));
	}
};