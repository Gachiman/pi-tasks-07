#ifndef PRJ
#define PRJ
class Project
{
public:
    virtual double ProjectSalary(int budget, double part) = 0;
};
#endif
