#pragma once
#ifndef WRK
#define WRK
class WorkTime
{
public:
	virtual int WorkTimeSalary(int base, int workTime) = 0;
};

#endif